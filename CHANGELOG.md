# 1.2.0

## Additions
* [#2](https://gitlab.com/foundry-azzurite/pings/issues/2) Add module author API


## Fixes
* [#1](https://gitlab.com/foundry-azzurite/pings/issues/1) Fix pings not working after scene has been switched

# 1.1.0
* Add setting to disallow users with a specific permission or lower from moving the screen

# 1.0.1
* Remove debug logging

# 1.0.0
* Initial Release
